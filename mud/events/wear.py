
from .event import Event2
from .info  import InfoEvent

class WearEvent(Event2):
	NAME = "wear"
	def perform(self):
		if not self.object.has_prop("wearable"):
			self.fail()
			return self.inform("wear.failed")
		self.inform("wear")