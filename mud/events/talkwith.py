from .event import Event2

class TalkWithEvent(Event2):
    NAME = "talk-with"

    def perform(self):
        if not self.object.has_prop("pnj"):
            self.fail()
            return self.inform("talk-with.failed")
        self.inform("talk-with")
