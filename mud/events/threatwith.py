# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3


class ThreatWithEvent(Event3):
    NAME = "threat-with"

    def perform(self):
        if not self.object.has_prop("menacable"):
            self.fail()
            return self.inform("threat-with.failed")
        self.inform("threat-with")