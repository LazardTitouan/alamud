from .action import Action2
from mud.events import WearEvent

class WearAction(Action2):
    EVENT = WearEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "wear"
