
from .action import Action2,Action3
from mud.events import TalkWithEvent

class TalkWithAction(Action2):
    EVENT = TalkWithEvent
    ACTION = "talk-with"
    RESOLVE_OBJECT = "resolve_for_operate"